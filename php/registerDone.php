<section>
    <div class="icon fa-check highlightIconOrange highlightIconBoxAni"></div>
    <article class="container box highlight">
        <header>
            <h4>Vielen Dank für deine Anmeldung. Du erhälst <b>in Kürze</b> weitere Informationen per E-Mail</h4>
        </header>
    </article>
</section>