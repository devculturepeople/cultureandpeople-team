<?php
if(isset($_SESSION['userid'])){	
	if(!isset($_POST['order']) && isset($_GET['restore'])){
		$transfer = false;
		
		$stmt = $db->prepare("SELECT event FROM opentransactionselements WHERE transaction IN (SELECT id FROM opentransactions WHERE user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) <= NOW())");
		$stmt->bind_param("i",$_SESSION['userid']);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			$stmt->bind_result($event);
			
			$stmtInsert = $db->prepare("INSERT INTO baskets (event, user) VALUES (?,?)");
			
			while($stmt->fetch()){
				$stmtInsert->bind_param("ii",$event,$_SESSION['userid']);
				$stmtInsert->execute();
			}
			
			$stmtInsert->close();
			$transfer = true;
		}
		$stmt->close();
		
		$stmt = $db->prepare("SELECT value, name FROM opentransactionscoupons WHERE transaction IN (SELECT id FROM opentransactions WHERE user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) <= NOW())");
		$stmt->bind_param("i",$_SESSION['userid']);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			$stmt->bind_result($couponvalue,$couponname);
			
			$stmtInsert = $db->prepare("INSERT INTO basket_coupon (value, user, name) VALUES (?,?,?)");
			
			while($stmt->fetch()){
				$stmtInsert->bind_param("iis",$couponvalue,$_SESSION['userid'],$couponname);
				$stmtInsert->execute();
			}
			
			$stmtInsert->close();
			$transfer = true;
		}
		$stmt->close();
		
		$stmt = $db->prepare("SELECT coupon FROM opentransactionsusedcoupons WHERE transaction IN (SELECT id FROM opentransactions WHERE user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) <= NOW())");
		$stmt->bind_param("i",$_SESSION['userid']);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			$stmt->bind_result($couponnr);
			
			$stmtInsert = $db->prepare("INSERT IGNORE INTO basket_coupon_use (coupon, user) VALUES (?,?)");
			
			while($stmt->fetch()){
				$stmtInsert->bind_param("ii",$couponnr,$_SESSION['userid']);
				$stmtInsert->execute();
			}
			
			$stmtInsert->close();
			$transfer = true;
		}
		$stmt->close();
		
		if($transfer){
			deleteOpenTransaction();
		}
	}
	
	if(isset($_POST['delete'])){
		$basketid = array_keys($_POST['delete'])[0];
		$couponCheck = explode("_", $basketid);
		if($couponCheck[0]=="c" && is_numeric($couponCheck[1])){
			$stmt = $db->prepare("DELETE FROM basket_coupon WHERE user = ? AND id = ?");
			$stmt->bind_param("ii", $_SESSION['userid'], $couponCheck[1]);
			$stmt->execute(); 
			$stmt->close();
			
		}elseif($couponCheck[0]=="r" && is_numeric($couponCheck[1])){
			$stmt = $db->prepare("DELETE FROM basket_coupon_use WHERE user = ? AND coupon = ?");
			$stmt->bind_param("ii", $_SESSION['userid'], $couponCheck[1]);
			$stmt->execute(); 
			$stmt->close();
			
		}elseif(is_numeric($basketid)){
			$stmt = $db->prepare("DELETE FROM baskets WHERE user = ? AND event = ?");
			$stmt->bind_param("ii", $_SESSION['userid'], $basketid);
			$stmt->execute(); 
			$stmt->close();
			
		}
	}
	
	if(isset($_POST['addcoupon'])){
		if(isset($_POST['couponnumber']) && ctype_alnum($_POST['couponnumber'])){
			$stmt = $db->prepare("SELECT id, inf, value FROM coupons WHERE code = ?");
			$stmt->bind_param("s",$_POST['couponnumber']);
			$stmt->execute();
			$stmt->store_result();
			if($stmt->num_rows>0){
				$stmt->bind_result($couponid, $couponinf, $couponvalue);
				$stmt->fetch();
				$stmt->close();
				
				if($couponinf == 1){
					$stmt = $db->prepare("SELECT SUM(value) FROM used_coupons WHERE coupon = ? AND user = ?");
					$stmt->bind_param("ii", $couponid, $_SESSION['userid']);	
					
					$stmt2 = $db->prepare("SELECT SUM(value) FROM opentransactionsusedcoupons AS otuc LEFT JOIN opentransactions AS ot ON otuc.transaction = ot.id WHERE coupon = ? AND user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) > NOW()");
					$stmt2->bind_param("ii", $couponid, $_SESSION['userid']);	
				}else{
					$stmt = $db->prepare("SELECT SUM(value) FROM used_coupons WHERE coupon = ?");
					$stmt->bind_param("i", $couponid);
					
					$stmt2 = $db->prepare("SELECT SUM(value) FROM opentransactionsusedcoupons AS otuc LEFT JOIN opentransactions AS ot ON otuc.transaction = ot.id WHERE coupon = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) > NOW()");
					$stmt2->bind_param("i", $couponid);
				}
				
				$stmt->execute();
				$stmt->bind_result($usedcoupon);
				$stmt->fetch();
				$stmt->close();
				
				$stmt2->execute();
				$stmt2->bind_result($otUsedCoupon);
				$stmt2->fetch();
				$stmt2->close();
				
				$currentCouponValue = $couponvalue-$usedcoupon-$otUsedCoupon;
				if($currentCouponValue == 0){
					$error['basket'] = "<article class='container box highlight'><header>Der Gutscheincode ist <b>bereits benutzt</b> worden</header></article>";
				}else{
					$stmt = $db->prepare("INSERT IGNORE INTO basket_coupon_use (coupon, user) VALUES (?,?)");
					$stmt->bind_param("ii",$couponid, $_SESSION['userid']);
					$stmt->execute();
					$stmt->close();
				}
			}else{
				$stmt->close();
				$error['basket'] = "<article class='container box highlight'><header>der Gutscheincode ist<b> ungültig</b></header></article>";
			}
		}else{
			$error['basket'] = "<article class='container box highlight'><header>der Gutscheincode ist<b> ungültig</b></header></article>";
		}
	}
	
	$price = 0;
	
	$output = "";
	
	$delete = array();
	
	$basket = array();
	
	$coupons = array();
	
	$stmtInsert = $db->prepare("INSERT INTO baskets (event, user) VALUES (?,?)");
	$stmtDelete = $db->prepare("DELETE FROM baskets WHERE event = ? AND user = ? LIMIT ?");
	
	$stmt = $db->prepare("SELECT COUNT(e.id), e.id, e.name, e.date, e.groupsize_max, e.price, b.count, ba.id AS basketid FROM baskets AS ba 
							LEFT JOIN events AS e ON e.id = ba.event 
							LEFT JOIN (SELECT SUM(count) AS count, event FROM
											((SELECT COUNT(user) AS count, event FROM bookings WHERE state = 0 GROUP BY event)
											UNION
											(SELECT COUNT(user) AS count, event FROM opentransactionselements AS ote LEFT JOIN opentransactions AS ot ON ot.id = ote.transaction WHERE DATE_ADD(ot.time, INTERVAL 15 MINUTE) > NOW() GROUP BY event)
											) AS t
										GROUP BY event) AS b ON b.event=e.id 
										WHERE ba.user = ? AND e.online = 1 GROUP BY e.id ORDER BY e.id");
	$stmt->bind_param("i",$_SESSION['userid']);
	$stmt->execute();
	$stmt->bind_result($r['countb'],$r['id'],$r['name'],$r['date'],$r['max'],$r['price'],$r['count'],$r['basketid']);
	$stmt->store_result();
	
	
	while($stmt->fetch()){
		
		
		if(isset($_POST['count']) && !isset($_POST['delete']) && ($r['max']==0 || $r['max']-$r['count']>=$_POST['count'][$r['id']])){
			if($r['countb']>$_POST['count'][$r['id']]){
				$count = $r['countb']-$_POST['count'][$r['id']];
				$stmtDelete->bind_param("iii",$r['id'],$_SESSION['userid'],$count);
				$stmtDelete->execute();
				
				$r['countb'] = $_POST['count'][$r['id']];
				
			}elseif($r['countb']<$_POST['count'][$r['id']]){
				$stmtInsert->bind_param("ii",$r['id'],$_SESSION['userid']);
				for($i=0;$i<($_POST['count'][$r['id']]-$r['countb']);$i++){
					$stmtInsert->execute();
				}
				
				$r['countb'] = $_POST['count'][$r['id']];
				
			}
		}
		
		$output .= "<div class='row' style='margin-bottom: 1em;'>
                        <div class='5u'>
                            <footer>
                                <div class='12u'><b><a style='text-decoration: none;color: #000; font-weight: 700;' href='./?event=$r[id]'>$r[name]</a></b></div>
                            </footer>
                        </div>
                        <div class='3u'>
                            <footer>
                                <div class='12u'>".date("d.m.Y H:i", strtotime($r['date']))." Uhr</div>
                            </footer>
                        </div>";
		if($r['count']>=$r['max'] && $r['max']!=0){
			$status = "<article class='highlight'><header>Dieses Event ist leider <b>ausgebucht</b> und wurde deshalb entfernt</header></article>";
			$delete[] = array($r['id'],$r['countb']);
		}elseif($r['max']!=0 && $r['max']-$r['count']<$r['countb']){
			$status = "<article class='container box highlight'><header>Dieses Event ist leider nur noch <b>".($r['max']-$r['count'])."x</b> verfügbar</header></article>";
			$delete[] = array($r['id'],$r['countb']-$r['max']+$r['count']);
			$r['countb'] = $r['max']-$r['count'];
			
		}elseif(strtotime($r['date'])<time()){
			$status = "<article class='highlight'><header>Dieses Event liegt <b>in der Vergangenheit</b> und wurde deshalb entfernt</header></article>";
			
			$delete[] = array($r['id'],$r['countb']);
			
		}else{
			$price += $r['price']*$r['countb'];
			$status ="<div class='2u mobileBasket' style='text-align:center;'>
                            <footer>
                                <button class='deleteButton' type='submit' name='delete[$r[id]]'>
                                    <i class='fa fa-times'></i> löschen
                                </button>
                            </footer>
                      </div>
                       <div class='1u mobileBasket'' style='text-align:right;'>
                            <footer>
                                <div class='12u'>".$r['price']."€</div>
                            </footer>
                        </div> ";}
					if($r['max']-$r['count']!=0 || $r['max'] == 0){
						$output .= "<div class='1u mobileBasket'>";
						$output .= "
                            <footer>
								<select name='count[$r[id]]' size='1' style='width:70px; font-size: 15px' onchange='this.form.submit();'>
								";
								if($r['max']==0){
									$max = 5;
								}else{
									$max = $r['max']-$r['count'];
								}
							for($i=1;$i<=$max;$i++){
								$output .= "<option value='$i' ".(($i==$r['countb'])?"selected":"").">$i</option>";
							}
						$output .= "</select>
							</footer></div>";
					$output .="$status";
					}else{
						$output .= "$status";
					}
        $output .="</div><div style='border-top:1px solid #ccc; height:1em;'></div>";
					
		$basket[] = array($r['id'],$r['countb']);
	}
	$stmt->close();
	$stmtDelete->close();
	$stmtInsert->close();
	$stmt = $db->prepare("SELECT id AS basketid, value, name FROM basket_coupon WHERE user = ?");
	$stmt->bind_param("i",$_SESSION['userid']);
	$stmt->execute();
	$stmt->bind_result($r['basketid'],$r['value'],$r['name']);
	$stmt->store_result();
	while($stmt->fetch()){
		$price += $r['value'];
		$coupons[] = array($r['value'],$r['name']);
		$output .= "<article class='row' style='margin-bottom: 1em;'>
                        <div class='9u' style='text-align:left;'>
                            <footer>
                                <div class='12u' style='color:#000;font-weight:700;'>
                                    <i class='fa fa-gift'></i> Gutschein im Wert von ".str_replace(".",",", $r['value'])."&euro; für <b>$r[name]</b>
                                </div>
                            </footer>
                            </div>
				        <div class='2u'>
                             <footer style='text-align:center;' >
                                 <button class='deleteButton' type='submit' name='delete[c_$r[basketid]]'>
                                    <i class='fa fa-times'></i> löschen
                                 </button>
                             </footer>
                        </div>
                        <div class='1u' style='text-align:right;'>
                            <footer>
                                <div class='12u'>$r[value]€</div>
                            </footer>
                      </div>
                      </article>
                      <div style='border-top:1px solid #ccc;'>&nbsp;</div>";}
	if(sizeof($basket)+sizeof($coupons)-sizeof($delete) != 0){
		
		$stmt = $db->prepare("SELECT id, value, inf, code FROM basket_coupon_use AS bcu LEFT JOIN coupons AS c ON c.id = bcu.coupon WHERE bcu.user = ?");
		$stmt->bind_param("i",$_SESSION['userid']);
		$stmt->execute();
		$stmt->bind_result($couponid, $couponvalue, $couponinf, $couponcode);
		$stmt->store_result();
		
		$usedCoupons = array();
		$deleteCoupon = array();
		
		while($stmt->fetch()){
			if($couponinf == 1){
				$stmt3 = $db->prepare("SELECT SUM(value) FROM used_coupons WHERE coupon = ? AND user = ?");
				$stmt3->bind_param("ii", $couponid, $_SESSION['userid']);	
				
				$stmt2 = $db->prepare("SELECT SUM(value) FROM opentransactionsusedcoupons AS otuc LEFT JOIN opentransactions AS ot ON otuc.transaction = ot.id WHERE coupon = ? AND user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) > NOW()");
				$stmt2->bind_param("ii", $couponid, $_SESSION['userid']);	
			}else{
				$stmt3 = $db->prepare("SELECT SUM(value) FROM used_coupons WHERE coupon = ?");
				$stmt3->bind_param("i", $couponid);
				
				$stmt2 = $db->prepare("SELECT SUM(value) FROM opentransactionsusedcoupons AS otuc LEFT JOIN opentransactions AS ot ON otuc.transaction = ot.id WHERE coupon = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) > NOW()");
				$stmt2->bind_param("i", $couponid);
			}
			
			$stmt3->execute();
			$stmt3->bind_result($usedcoupon);
			$stmt3->fetch();
			$stmt3->close();
			
			$stmt2->execute();
			$stmt2->bind_result($otUsedCoupon);
			$stmt2->fetch();
			$stmt2->close();
			
			$currentCouponValue = $couponvalue-$usedcoupon-$otUsedCoupon;
			
			$output .= "<article class='container box highlight'><header>Eingelöster Gutschein 1</header></article>";
			
			if($currentCouponValue == 0){
				$deleteCoupon[] = $couponid;
				$output .= "<article class='container box highlight'><header>Dieser Gutschein ist <b>schon aufgebrauchtund</b> wurde deshalb entfernt</header></article>";
				
			}else{
				if($currentCouponValue > $price){
					$usedValue = $price;
				}else{
					$usedValue = $currentCouponValue;
				}
				$output .="<input class='basketbutton' type='submit' value='entfernen' name='delete[r_$couponid]'>
				<article class='container box highlight'><header>".number_format($usedValue, 2, ",", " ")."€</header></article>";
				
				$usedCoupons[] = array($couponid,$usedValue,$couponcode);
				$price = $price-$usedValue;
			}
			$output .= "</form></div>";
		}
		
		$stmt->close();
		
		if(sizeof($deleteCoupon)!=0){
			$stmt = $db->prepare("DELETE FROM basket_coupon_use WHERE user = ? AND coupon = ?");
			
			foreach($deleteCoupon as $couponId){
				$stmt->bind_param("ii",$_SESSION['userid'],$couponId);
				$stmt->execute();
			}
			
			$stmt->close();
		
		}
	
	}
		
	if(sizeof($delete)!=0){
		$stmt = $db->prepare("DELETE FROM baskets WHERE user = ? AND event = ? LIMIT ?");
		
		foreach($delete as $deleteArray){
			$stmt->bind_param("iii",$_SESSION['userid'],$deleteArray[0],$deleteArray[1]);
			$stmt->execute();
		}
		$stmt->close();
	
	}
	
	
	if(isset($_POST['order']) && sizeof($delete)==0 && sizeof($deleteCoupon)==0){
		if(!isset($_POST['agbchecked'])){
			$error['basket'] = "<article class='container box highlight'><header>Bitte akzeptiere die <b>AGBs</b></header></article>";

		}elseif($price == 0){
			$stmt = $db->query("SELECT bnr, rnr FROM fln WHERE id=1");
			$result = $stmt->fetch_assoc();
			$stmt->close();
			
			$rnr = "R_CP".date("y").$result['rnr'];
			$flnr = $result['rnr'];
			$flnr++;
			
			$stmt = $db->prepare("SELECT realname, mail, gender FROM profile WHERE id = ?");
			$stmt->bind_param("i",$_SESSION['userid']);
			$stmt->execute();
			$stmt->bind_result($realname, $mail, $gender);
			$stmt->fetch();
			$stmt->close();
			
			$stmt = $db->prepare("INSERT INTO invokes (number, payment, user, type) VALUES (?,3,?,0)");
			$stmt->bind_param("si", $rnr, $_SESSION['userid']);
			$stmt->execute();
			$invokeid = $stmt->insert_id;
			$stmt->close();
			
			if(sizeof($basket) > 0){
				$stmt = $db->prepare("SELECT e.id, e.date, e.name, e.meetinginformation, e.price FROM events AS e WHERE id = ?");
				$stmt2 = $db->prepare("INSERT INTO bookings (event, user, invoke, number) VALUES (?,?,?,?)");
				$invokeElements = array();
				
				foreach($basket as $eventArray){
				
					$stmt->bind_param("i",$eventArray[0]);
					$stmt->execute();
					$stmt->bind_result($eid,$edate,$ename,$emeetinginformation,$eprice);
					$stmt->store_result();
					$stmt->fetch();
					
					$bnr = "B_CP".date("y").$result['bnr'];
					$stmt2->bind_param("iiis", $eid, $_SESSION['userid'], $invokeid,$bnr);
					for($i=0;$i<$eventArray[1];$i++){
						$stmt2->execute();	
					}
					$result['bnr']++;
					$invokeElements[] = array($ename, $edate, $emeetinginformation, $eprice, $bnr, $eventArray[1]);
					
				}
				$stmt2->close();
				$stmt->close();
			}
			
			$couponAttachments = array();
			
			if(sizeof($coupons) > 0){
				include_once "coupon.php";
				$couponCodes = generateCouponCodes(sizeof($coupons));
			
				$stmt = $db->prepare("INSERT INTO coupons (code, invoke, inf, value, name) VALUES (?,?,0,?,?)");
				
				$couponNr = 0;
				
				for($i=0; $i<sizeof($coupons); $i++){
					$bnr = "B_CP".date("y").$result['bnr'];
					$stmt->bind_param("sids", $couponCodes[$couponNr], $invokeid, $coupons[$i][0],$coupons[$i][1]);
					$stmt->execute();
					$result['bnr']++;
					$invokeElements[] = array("Gutschein im Wert von ".str_replace(".",",",$coupons[$i][0])."EUR für ".$coupons[$i][1], false, $couponCodes[$couponNr], $coupons[$i][0], $bnr,1);
					
					
					
					new Coupon($couponCodes[$couponNr],$coupons[$i][1],$coupons[$i][0],"now");
					$couponAttachments[] = "coupons/coupon_$couponCodes[$couponNr].pdf";
					
					$couponNr++;
				}
				
				$stmt->close();
				
				
			}		
			
			if(sizeof($usedCoupons)>0){
				$stmt = $db->prepare("INSERT INTO used_coupons (user, coupon, value, invoke) VALUES (?,?,?,?)");
							
				for($i=0; $i<sizeof($usedCoupons); $i++){
					//$bnr = "B_CP".date("y").$result['bnr'];
					$stmt->bind_param("sidi", $_SESSION['userid'], $usedCoupons[$i][0],  $usedCoupons[$i][1], $invokeid);
					$stmt->execute();
					//$result['bnr']++;
					$invokeElements[] = array("Eingelöster Gutschein", false, $usedCoupons[$i][2], -$usedCoupons[$i][1], false, 1);
				}
				
				$stmt->close();	
				
			}
			
			//print_r($invokeElements);
							
			include "mails/ordermail.php";
			$ordermail = new orderMail($realname, $gender, $rnr, $invokeElements, $mail, 3, false);
			htmlmail($mail, "Deine Bestellung bei Culture & People",$ordermail,$couponAttachments);
			
			$ordermail = new orderMail($realname, 0, $rnr, $invokeElements, $mail, 3, false, true);
			if($testMode){
				htmlmail("choooom@gmx.de", "Neue Bestellung",$ordermail);
			}else{
				htmlmail("info@cultureandpeople.de", "Neue Bestellung",$ordermail);
			}
			
			$stmt = $db->prepare("UPDATE fln SET bnr=?, rnr=? WHERE id=1");
			$stmt->bind_param("ii", $result['bnr'],$flnr);
			$stmt->execute();
			$stmt->close();
			
			$stmt = $db->prepare("DELETE FROM baskets WHERE user = ?");
			$stmt->bind_param("i",$_SESSION['userid']);
			$stmt->execute();
			$stmt->close();
			
			$stmt = $db->prepare("DELETE FROM basket_coupon WHERE user = ?");
			$stmt->bind_param("i",$_SESSION['userid']);
			$stmt->execute();
			$stmt->close();
			
			$stmt = $db->prepare("DELETE FROM basket_coupon_use WHERE user = ?");
			$stmt->bind_param("i",$_SESSION['userid']);
			$stmt->execute();
			$stmt->close();
			
			header("Location: ?paymentsuccess");
		}else{
		
			if(isset($_POST['payment_method']) && $_POST['payment_method'] >= 0 && $_POST['payment_method'] <= 2){
				
				$stmt = $db->query("SELECT rnr FROM fln WHERE id=1");
				$result = $stmt->fetch_assoc();
				$stmt->close();
				
				$rnr = "R_CP".date("y").$result['rnr'];
				$flnr = $result['rnr'];
				$flnr++;
				
				//payment_method: 0=> PayPal; 1=> Sofort; 2=>KreditKarte
				if($testMode){
					$token_correct = true;
					$token = 12345;
				}else{
				
					if($_POST['payment_method']==0){
						
					
						include "payments/paypal/AuthorizePaymentUsingPayPal.php";
						$payment = createToken($price, $rnr);
						
						if($payment != false)
						{
							$token_correct = true;
							$url = urldecode($payment->getApprovalLink()."&useraction=commit");
							$token = $payment->getID();
							
						}else{
							$error['basket'] = "<br><br><article class='container box highlight'><header>Es ist ein <b>unbekannter Fehler</b> aufgetreten<br>Sollte dieses Problem weiter bestehen <a href='./?contact'>schreibe uns bitte</a> eine Nachricht.</header></article>";
						}
						
						
					}elseif($_POST['payment_method']==1){
						 include "payments/sofort/sofort.php";
						 $sofortPayment = createSofortPayment($price, $rnr);
						 //var_dump($sofortPayment);
						 if($sofortPayment != false){
							 $url = urldecode($sofortPayment[0]);
							 $token = $sofortPayment[1];
							 $token_correct = true;
						 }
					}elseif($_POST['payment_method']==2){
						$token = "CP-".md5(uniqid($rnr,true));
						$token_correct = true;
					}
				}
				
				if(isset($token_correct)){
					deleteOpenTransaction();
					$stmt = $db->prepare("INSERT INTO opentransactions (token, paymentmethod,user,invokeid, amount) VALUES (?,?,?,?,?)");
					$stmt->bind_param("siisd",$token, $_POST['payment_method'], $_SESSION['userid'], $rnr, $price);
					$stmt->execute();
					$transactionid = $stmt->insert_id;
					$stmt->close();
					
					if(sizeof($basket)>0){
						
						$stmt = $db->prepare("INSERT INTO opentransactionselements (transaction, event) VALUES (?,?)");
						
						foreach($basket as $eventArray){
							$stmt->bind_param("ii", $transactionid, $eventArray[0]);
							for($i=0;$i<$eventArray[1];$i++){
								$stmt->execute();
							}
						}
						
						$stmt->close();
						
						$stmt = $db->prepare("DELETE FROM baskets WHERE user = ?");
						$stmt->bind_param("i",$_SESSION['userid']);
						$stmt->execute();
						$stmt->close();
					
					}
					
					if(sizeof($coupons)>0){
						
						$stmt = $db->prepare("INSERT INTO opentransactionscoupons (transaction, value, name) VALUES (?,?,?)");
						
						foreach($coupons as $coupon){
							$stmt->bind_param("ids", $transactionid, $coupon[0], $coupon[1]);
							$stmt->execute();
						}
						
						$stmt->close();
						
						$stmt = $db->prepare("DELETE FROM basket_coupon WHERE user = ?");
						$stmt->bind_param("i",$_SESSION['userid']);
						$stmt->execute();
						$stmt->close();
					
					}
					
					if(sizeof($usedCoupons)>0){
						
						$stmt = $db->prepare("INSERT INTO opentransactionsusedcoupons (transaction, coupon, value) VALUES (?,?,?)");
						
						foreach($usedCoupons as $coupon){
							$stmt->bind_param("iii", $transactionid, $coupon[0], $coupon[1]);
							$stmt->execute();
						}
						
						$stmt->close();
						
						$stmt = $db->prepare("DELETE FROM basket_coupon_use WHERE user = ?");
						$stmt->bind_param("i",$_SESSION['userid']);
						$stmt->execute();
						$stmt->close();
						
					}
					
					$stmt = $db->prepare("UPDATE fln SET rnr=? WHERE id=1");
					$stmt->bind_param("i",$flnr);
					$stmt->execute();
					$stmt->close();
					
					if($testMode){
						if($_POST['payment_method']==0){
							$url = "?paymentsuccess&paypal";
							
						}elseif($_POST['payment_method']==1){
							$url = "?paymentsuccess&sofort";
							
						}elseif($_POST['payment_method']==2){
							$url = "?paymentsuccess&creditcard";
						}
						
						//header("Location: ".$url);
					}else{
					
						if($_POST['payment_method']==0){
							header("Location: ".$url);
							
						}elseif($_POST['payment_method']==1){
							header("Location: ".$url);
							
						}elseif($_POST['payment_method']==2){
							include "payments/telecash/paymentFile.php";
							$redirectForm = getRedirectionForm($price, $rnr, $token);
						}
					
					}
				}
				
			}else{
				$error['basket'] = "<article class='container box highlight'><header>Bitte wähle eine <b>Zahlungsart</b> aus</header></article>";
			}
		}
	}
	
	if(isset($_POST['order']) && sizeof($delete)!=0){
		$error['basket'] = "<article class='container box highlight'><header>Es sind leider <b>nicht mehr alle Artikel verfügbar</b>. Bitte überprüfe die <b>aktuelle Bestellung</b></header></article>";
	}
}
?>