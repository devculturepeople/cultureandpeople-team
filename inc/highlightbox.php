<!-- START DONATE BOX Loop -->
<br>
<section class="container" style="margin-bottom: 50px !important;">
    <div class="icon fa-heart highlightHeart"></div>
    <article class="box highlight">
        <h4> Mit jedem gebuchten Event helfen wir <a href="http://www.sos-kinderdoerfer.de/" target="_blank"><strong>SOS-Kinderdorf weltweit</strong></a>.</br>
            Gerne kannst du auch privat <a href="http://www.sos-kinderdoerfer.de/helfen/spenden" target="_blank"><strong>spenden</strong></a></h4>
    </article>
</section>
<!-- END DONATE BOX Loop -->