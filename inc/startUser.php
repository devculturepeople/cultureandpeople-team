<section>
    <div class="icon fa-user highlightIcon"></div>
    <article class="box highlight">
     Herzlich Willkommen zurück <b><?php echo $userinformation['username']; ?></b>
        <br>
    </article>
</section>
<section>
    <div class="icon fa-eye highlightIcon"></div>
    <article class="box highlight">
        gefällt dir unsere <b>neue Webseite</b> oder hast du <b>Verbesserungsvorschläge</b>?
        <ul class="actions" style="margin-top: 1em; text-align: center;">
            <li>
                <a href="?contact" class="basketButton button icon fa-pencil" target="_self">schreibe uns</a>
            </li>
        </ul>
    </article>
</section>
<br>