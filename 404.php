<?php
	
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	header("Content-Type: text/html; charset=utf-8");
	include "inc/db_open.php";
	require_once("inc/page_keys.php");
	require_once("inc/core.php");
	require_once("inc/randomFact.php");
	
	if(isset($_GET['logout'])){
		session_destroy();
		unset($_SESSION['userid']);
	}
	
	if(isset($_POST['login'])){
		$name = strtolower($_POST['name']);
		if(trim($name)=="" || !ctype_alnum($name) || trim($_POST['password'])==""){
			$error['login'] = "Benutzername oder Passwort falsch! <a href='./?resetPassword' style='color: #fff;'>Passwort vergessen?</a>";
		}else{
			$password = hash("sha512",$PWSALT.$_POST['password']);
			$stmt = $db->prepare("SELECT id FROM profile WHERE username = ? AND password = ?");
			$stmt->bind_param("ss", $name, $password);
			$stmt->execute();
			
			$stmt->store_result();
			
			if($stmt->num_rows == 1){
				$stmt->bind_result($r['id']);
				$stmt->fetch();
				$_SESSION['userid'] = $r['id'];
			}else{
				$error['login'] = "Benutzername oder Passwort falsch! <a href='./?resetPassword'>Passwort vergessen?</a>";
			}
			$stmt->close();
		}
	}
	if(isset($_SESSION['userid'])){
		$stmt = $db->prepare("SELECT username FROM profile WHERE id = ?");
		$stmt->bind_param("i", $_SESSION['userid']);
		$stmt->execute();

		$stmt->store_result();
		if($stmt->num_rows == 1){
			$stmt->bind_result($userinformation['username']);
			$stmt->fetch();
			
		}else{
			session_destroy();
			unset($_SESSION['userid']);
		}
		$stmt->close();
	}
	
	if(isset($_GET['confirm'])){
		include "inc/confirmmail.php";
	}
	
	if(isset($_SESSION['userid'])){
		$pageKeys = new pageKeys(true);
	}else{
		$pageKeys = new pageKeys(false);
	}
	$id = $pageKeys->getPageId();
	
	if($id == 160){
		include "inc/basket_management.php";
	}
	
	if(isset($_GET['paymentsuccess']) || isset($_GET['paymentcancel'])){
		include "inc/finishorder_management.php";
	}
	
	if(isset($_GET['deletetransaction'])){
		deleteOpenTransaction();
	}
	
	if($id==1 || $id == 160){
		$stmt = $db->prepare("SELECT id FROM opentransactions WHERE user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) < NOW()");
		$stmt->bind_param("i",$_SESSION['userid']);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows > 0){
			$stmt->bind_result($otID);
			$stmt->fetch();
			$confirmmessage = "Sie haben Ihre letzte Bestellung nicht abgeschlossen. Möchten Sie Ihren Warenkorb wiederherstellen? <a href='?basket&restore'>Ja</a> <a href='?deletetransaction'>Nein</a>";
		}
		$stmt->close();
	}
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Culture & People</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Treffen Sie neue Freunde und Bekannte oder auch den Partner für das Leben bei einer Vernissage, bei einer modernen Theaterauff&uuml;hrung oder einem Kochkurs.">
        <meta name="keywords" content="Culture&amp;People, Cultureandpeople, Blinddate, M&uuml;nchen, Blind date, Freizeit, Kunst, Kultur, Veranstaltungen, Vernisage, Funsport, Events, Menschen, treffen">

		<meta name="DC.Language" content="de">
		<meta property="og:title" content="Culture &amp; People"/>
        <meta property="og:description" content="Treffen Sie neue Freunde und Bekannte oder auch den Partner für das Leben bei einer Vernissage, bei einer modernen Theaterauff&uuml;hrung oder einem Kochkurs.">
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://www.cultureandpeople.de"/>
		<meta property="og:locale" content="de_DE"/>
		<meta property="og:image" content="images/logo.png"/>

		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<script src="js/headerToggleScroll.js"></script>
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.3.0/balloon.min.css">
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	
<!-- START Google Analytics Code -->
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-64483921-1', 'auto');
				ga('send', 'pageview');
			</script>
<!--END Google Analytics Code -->

<!-- Preloader -->
<script type="text/javascript">
    //<![CDATA[
        $(window).on('load', function() { // makes sure the whole site is loaded 
            $('#status').fadeOut(); // will first fade out the loading animation 
            $('#preloader').delay(1350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
            $('body').delay(1350).css({'overflow':'visible'});
        })
    //]]>
</script>

	<body class="homepage">

	<!--START Preloader -->
	<div id="preloader">
		<div id="status"><img width="99" height="69" style="margin:-3.4em 0 0 3.8em;" src="../images/logo.png" alt="culture-and-peoples-logo" /></div>  </div>
	<!--END Preloader -->

	<!--START Header -->
	<section id="header-wrapper">
		<div id="header">
			<section class="container">
				<script type="text/javascript">
				function clearDefault(a){if(a.defaultValue==a.value){a.value=""}};
			</script>
				<style>
         #header-wrapper #header .row>* {
            padding: 0px !important;
            height: auto;
        }
        </style>
				<div class="row" style="display: inline; margin: 0px !important;">

					<!--START Logo -->
					<div class="1u">
						<div class="logoFix">
							<a href="./">
								<img src="../images/logo-smal.png" alt="culture-and-peoples-logo" />
							</a>
						</div>
					</div>
					<!--END Logo -->

					<!--START Nav -->
					<div class="5u">
						<nav id="nav">
							<?php include "inc/events_side.php";
                         ?>
							<?php
                                        /*$query = $db->query("SELECT c.comment, c.id, c.image FROM comment AS c JOIN (SELECT (RAND() * (SELECT MAX(id) FROM comment WHERE online = 1)) AS id) AS random WHERE c.online = 1 AND c.id >= random.id ORDER BY c.id ASC LIMIT 1");
							if($query->num_rows>0){
							$result = $query->fetch_assoc();
							echo "<div class='row'><div class='12u'>Rezensionen</div>";
							if($result['image']!=""){
							echo "<img src='img/feedback/$result[id]$result[image]' width='100%' height='*'>";
							}
							echo "$result[comment]</div>";
							}
							*/
							?>
						</nav>
					</div>
					<!--END Nav -->

					<!--START right Navbar -->

					<?php
                                    if(isset($_SESSION['userid'])){
                                        include "inc/header/accountinformation.php";
                                    }else{
                                        include "inc/header/login.php";
                                    }
                            ?>


					<!--END right Navbar -->
			</section>
		</div>
	</section>
<!--END Header -->
	<!--START Main Content -->

			<div id="main-wrapper">
				<div id="main" class="container">
					<div class="12u">
						<br>
					<section style="margin-bottom: 50px;">
						<div class="container" style="text-align:center; margin-bottom: -26px;">
						<div class="icon fa fa-gear fa-spin fa-3x fa-fw"></div></div>
						<article class="box highlight">
							<h2>Error <b>404</b></h2>
							Seite leider nicht gefunden<br>
							<a href="/index.php"><b>zurück zur Startseite</b></a>
						</article>
					</section>
					</div>
				</div>
		<!--END Main Content -->
			</div>
<!--END Content -->

<!--START Footer -->
		<div id="footer-wrapper">
			<div id="footer" class="container">
			<header>
					<h2>Fragen? <b>Fragen!</b></h2>
				</header>
				<div class="row">
					<div class="12u">
						<section>
							<div class="row">
								<div class="7u">
								<style>
									   #map {
										height: 166px;
										width: 100%;
										-webkit-border-radius: 4px;
										-moz-border-radius: 4px;
										border-radius: 4px;
									   }
									   #footerMapsContent {
										color: #000 !important;
										width: 206px;
										}
									</style>
									<div id="map"></div>
										<script>
										  function initMap() {
											 var isDraggable = !('ontouchstart' in document.documentElement);
												var mapOptions = {
												  draggable: isDraggable,
												  scrollwheel: false
												};
											var uluru = {lat: 48.1779023113984, lng: 11.469731519049091};
											var map = new google.maps.Map(document.getElementById('map'), {
											  zoom: 16,
											  center: uluru
											});
											var contentString = '<div id="footerMapsContent">'+
												  '<h4>Culture & People UG</h4>'+
												  'Bauseweinallee 127<br>'+
												  '80999 München'+
												  '</div>';

											var infowindow = new google.maps.InfoWindow({
												content: contentString
											  });
											
											var marker = new google.maps.Marker({
											  position: uluru,
											  map: map,
											  title: 'Culture & People UG'											  
											});
											 marker.addListener('click', function() {
											infowindow.open(map, marker);
										  });
										  }
									</script>
											<script async defer
											src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_SBBAc2Ez031qzK0JhkEERxYCh36S1SQ&callback=initMap">
											</script>									
								</div>
								<div class="2u">
									<ul class="icons">
										<li class="icon fa-eye footerIcon"><a href="./?agb">AGB</a></li>
										<li class="icon fa-gavel footerIcon"><a href="./?impress">Impressum</a></li>
										<li class="icon fa-bullhorn footerIcon"><a href="./?contact">Kontakt</a></li>
									</ul>
								</div>
								<div class="3u">
									<ul class="icons">
										<li class="icon fa-envelope footerIcon">
											<a href="#">info [at] culutreandpeople.de</a>
										</li>
										<li class="icon fa-twitter footerIcon">
											<a href="https://www.facebook.com/cultureandpeople.de">Culture&People auf Twitter</a>
										</li>
										<li class="icon fa-facebook footerIcon">
											<a href="https://www.facebook.com/cultureandpeople.de">Culture&People auf Facebook</a>
										</li>
									</ul>
								</div>							
							</div>
						</section>
					</div>
				</div>
			</div>
			<div id="copyright" class="container">
				<ul class="links">
					<li style="color: #E3EAED;">
						Culture&People v1.2 &copy; 2016
					</li>
				</ul>
			</div>
		</div>		
<!--END Footer -->

	</body>

</html>