<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
header("Content-Type: text/html; charset=utf-8");
include "inc/db_open.php";
require_once("inc/page_keys.php");
require_once("inc/core.php");
require_once("inc/randomFact.php");

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['userid']);
}

if (isset($_POST['login'])) {
    $name = strtolower($_POST['name']);
    if (trim($name) == "" || !ctype_alnum($name) || trim($_POST['password']) == "") {
        $error['login'] = "Benutzername oder Passwort falsch! <a href='./?resetPassword' style='color: #fff;'>Passwort vergessen?</a>";
    } else {
        $password = hash("sha512", $PWSALT . $_POST['password']);
        $stmt = $db->prepare("SELECT id FROM profile WHERE username = ? AND password = ?");
        $stmt->bind_param("ss", $name, $password);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows == 1) {
            $stmt->bind_result($r['id']);
            $stmt->fetch();
            $_SESSION['userid'] = $r['id'];
        } else {
            $error['login'] = "Benutzername oder Passwort falsch! <a href='./?resetPassword'>Passwort vergessen?</a>";
        }
        $stmt->close();
    }
}
if (isset($_SESSION['userid'])) {
    $stmt = $db->prepare("SELECT username FROM profile WHERE id = ?");
    $stmt->bind_param("i", $_SESSION['userid']);
    $stmt->execute();

    $stmt->store_result();
    if ($stmt->num_rows == 1) {
        $stmt->bind_result($userinformation['username']);
        $stmt->fetch();

    } else {
        session_destroy();
        unset($_SESSION['userid']);
    }
    $stmt->close();
}

if (isset($_GET['confirm'])) {
    include "inc/confirmmail.php";
}

if (isset($_SESSION['userid'])) {
    $pageKeys = new pageKeys(true);
} else {
    $pageKeys = new pageKeys(false);
}
$id = $pageKeys->getPageId();

if ($id == 160) {
    include "inc/basket_management.php";
}

if (isset($_GET['paymentsuccess']) || isset($_GET['paymentcancel'])) {
    include "inc/finishorder_management.php";
}

if (isset($_GET['deletetransaction'])) {
    deleteOpenTransaction();
}

if ($id == 1 || $id == 160) {
    $stmt = $db->prepare("SELECT id FROM opentransactions WHERE user = ? AND DATE_ADD(time, INTERVAL 15 MINUTE) < NOW()");
    $stmt->bind_param("i", $_SESSION['userid']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($otID);
        $stmt->fetch();
        $confirmmessage = "Sie haben Ihre letzte Bestellung nicht abgeschlossen. Möchten Sie Ihren Warenkorb wiederherstellen? <a href='?basket&restore'>Ja</a> <a href='?deletetransaction'>Nein</a>";
    }
    $stmt->close();
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Culture & People</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="Treffen Sie neue Freunde und Bekannte oder auch den Partner für das Leben bei einer Vernissage, bei einer modernen Theaterauff&uuml;hrung oder einem Kochkurs.">
    <meta name="keywords"
          content="Culture&amp;People, Cultureandpeople, Blinddate, M&uuml;nchen, Blind date, Freizeit, Kunst, Kultur, Veranstaltungen, Vernisage, Funsport, Events, Menschen, treffen">
    <meta name="theme-color" content="#83a0ac">
    <meta name="DC.Language" content="de">
    <meta property="og:title" content="Culture &amp; People"/>
    <meta property="og:description"
          content="Treffen Sie neue Freunde und Bekannte oder auch den Partner für das Leben bei einer Vernissage, bei einer modernen Theaterauff&uuml;hrung oder einem Kochkurs.">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://www.cultureandpeople.de"/>
    <meta property="og:locale" content="de_DE"/>
    <meta property="og:image" content="images/logo.png"/>

    <!-- START LOGO BROWSER FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="manifest" href="images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- END LOGO BROWSER FACICON -->

    <!--[if lte IE 8]>
    <script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/headerToggleScroll.js"></script>
    <script src="js/jquery.pwstrength.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <noscript>
        <link rel="stylesheet" href="css/skel.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/style-desktop.css"/>
    </noscript>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie/v8.css"/><![endif]-->
</head>

<!-- START Google Analytics Code -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-64483921-1', 'auto');
    ga('send', 'pageview');
</script>
<!--END Google Analytics Code -->

<!-- Preloader -->
<!-- <script type="text/javascript">
    //<![CDATA[
        $(window).on('load', function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(500).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(500).css({'overflow':'visible'});
        })
    //]]>
</script> -->
<body class="homepage">

<!--START Preloader -->
<!-- <div id="preloader">
    <div id="status"><img width="99" height="69" style="margin:-3.4em 0 0 3.8em;" src="images/logo.png" alt="culture-and-peoples-logo" /></div></div> -->
<!--END Preloader -->

<!--START Header -->
<section id="header-wrapper">
    <div id="header">
        <section class="container">
            <script type="text/javascript">
                function clearDefault(a) {
                    if (a.defaultValue == a.value) {
                        a.value = ""
                    }
                }
                ;
            </script>
            <style>
                #header-wrapper #header .row > * {
                    padding: 0px !important;
                    height: auto;
                }
            </style>
            <div class="row" style="display: inline; margin: 0px !important;">

                <!--START Logo -->
                <div class="1u">
                    <div class="logoFix">
                        <a href="./">
                            <img src="images/logo-smal.png" alt="culture-and-peoples-logo"/>
                        </a>
                    </div>
                </div>
                <!--END Logo -->

                <!--START Nav -->
                <div class="5u">
                    <nav id="nav">
                        <?php include "inc/events_side.php";
                        ?>
                        <?php
                        /*$query = $db->query("SELECT c.comment, c.id, c.image FROM comment AS c JOIN (SELECT (RAND() * (SELECT MAX(id) FROM comment WHERE online = 1)) AS id) AS random WHERE c.online = 1 AND c.id >= random.id ORDER BY c.id ASC LIMIT 1");
                        if($query->num_rows>0){
                            $result = $query->fetch_assoc();
                            echo "<div class='row'><div class='12u'>Rezensionen</div>";
                            if($result['image']!=""){
                                echo "<img src='img/feedback/$result[id]$result[image]' width='100%' height='*'>";
                            }
                            echo "$result[comment]</div>";
                        }
                        */
                        ?>
                    </nav>
                </div>
                <!--END Nav -->

                <!--START right Navbar -->

                <?php
                if (isset($_SESSION['userid'])) {
                    include "inc/header/accountinformation.php";
                } else {
                    include "inc/header/login.php";
                }
                ?>


                <!--END right Navbar -->
        </section>
    </div>
</section>
<!--END Header -->

<!--START Main Content -->

<div id="main-wrapper">
    <div id="main" class="container">
        <!--START Greeting Area -->
        <?php if (isset($_SESSION['userid']) && (($id == 1) || ($id == 100))) {
            include "inc/startUser.php";
        } elseif ($id == 1) {
            include "inc/start.php";
        }
        ?>

        <!--END Greeting Area -->
        <?php
        switch ($id) {
            case 1:
                include "php/main.php";
                break;
            case 2:
                include "php/description.php";
                break;
            case 3:
                include "php/impress.php";
                break;
            case 4:
                include "php/agb.php";
                break;
            case 5:
                include "php/contact.php";
                break;
            case 10:
                include "php/register.php";
                break;
            case 11:
                include "php/resetpassword.php";
                break;
            case 12:
                include "php/changepassword.php";
                break;
            case 100:
                include "php/profile.php";
                break;
            case 101:
                include "php/editprofile.php";
                break;
            case 102:
                include "inc/startUser.php";
                break;
            case 110:
                include "php/account.php";
                break;
            case 150:
                include "php/settings.php";
                break;
            case 160:
                include "php/basket.php";
                break;
            case 200:
                include "php/messages.php";
                break;
            case 300:
                include "php/event.php";
                break;
            case 350:
                include "php/coupon.php";
                break;
            case 400:
                include "php/events.php";
                break;
            case 500:
                include "php/calendar.php";
                break;
            //case 500:
            //	include "php/feedback.php";
            //break;
            default:
                include "php/main.php";
                break;
        }
        ?>
    </div>
    <!--END Main Content -->

    <!--START features Content -->
    <?php if ($id == 1) {
        include "php/features.php";
    }
    ?>
    <!--END features Content -->

    <?php if ($id == 1) {
        include "inc/highlightbox.php";
    }
    ?>

</div>
<!--END Content -->

<!--START Footer -->
<div id="footer-wrapper">
    <div id="footer" class="container">
        <header>
            <h2><b>NEUGIERIG</b> GEWORDEN?</h2>
        </header>
        <div class="row">
            <div class="12u">
                <section>
                    <div class="row">
                        <div class="6u">
                            <style>
                                #map {
                                    height: 166px;
                                    width: 100%;
                                    -webkit-border-radius: 4px;
                                    -moz-border-radius: 4px;
                                    border-radius: 4px;
                                }

                                #footerMapsContent {
                                    color: #000 !important;
                                    width: 206px;
                                }
                            </style>
                            <div id="map"></div>
                            <script>
                                function initMap() {
                                    var isDraggable = !('ontouchstart' in document.documentElement);
                                    var mapOptions = {
                                        draggable: isDraggable,
                                        scrollwheel: false
                                    };
                                    var uluru = {lat: 48.1779023113984, lng: 11.469731519049091};
                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 16,
                                        center: uluru
                                    });
                                    var mapOptions = {
                                        zoom: 11,
                                        center: new google.maps.LatLng(55.6468, 37.581),
                                        mapTypeControlOptions: {
                                            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                                        }
                                    };
                                    var contentString = '<div id="footerMapsContent">' +
                                        '<h4>Culture & People UG</h4>' +
                                        'Bauseweinallee 127<br>' +
                                        '80999 München' +
                                        '</div>';

                                    var infowindow = new google.maps.InfoWindow({
                                        content: contentString
                                    });

                                    var marker = new google.maps.Marker({
                                        position: uluru,
                                        map: map,
                                        title: 'Culture & People UG'
                                    });
                                    var styles = [
                                        {
                                            stylers: [
                                                {hue: "#00ffe6"},
                                                {saturation: -20}
                                            ]
                                        }, {
                                            featureType: "road",
                                            elementType: "geometry",
                                            stylers: [
                                                {lightness: 100},
                                                {visibility: "simplified"}
                                            ]
                                        }, {
                                            featureType: "road",
                                            elementType: "labels",
                                            stylers: [
                                                {visibility: "off"}
                                            ]
                                        }
                                    ];
                                    marker.addListener('click', function () {
                                        infowindow.open(map, marker);
                                    });
                                }
                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_SBBAc2Ez031qzK0JhkEERxYCh36S1SQ&callback=initMap">
                            </script>
                        </div>
                        <div class="2u">
                            <ul class="icons">
                                <li class="icon fa-balance-scale footerIcon"><a href="./?agb">AGB</a></li>
                                <li class="icon fa-gavel footerIcon"><a href="./?impress">Impressum</a></li>
                                <li class="icon fa-bullhorn footerIcon"><a href="./?contact">Kontakt</a></li>
                            </ul>
                        </div>
                        <div class="4u">
                            <ul class="icons">
                                <li class="icon fa-envelope footerIcon">
                                    <a href="#">info [at] culutreandpeople.de</a>
                                </li>
                                <li class="icon fa-twitter footerIcon">
                                    <a href="https://www.twitter.com/Phexis2015" target="_blank">Culture&People auf
                                        Twitter</a>
                                </li>
                                <li class="icon fa-facebook footerIcon">
                                    <a href="https://www.facebook.com/cultureandpeople.de" target="_blank">Culture&People
                                        auf Facebook</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div id="copyright" class="container">
        <ul class="links">
            <li style="color: #E3EAED;">
                Culture&People v1.2 &copy; 2016
            </li>
        </ul>
    </div>
</div>
<!--END Footer -->
<!--START Show more Categorys Function -->
<script>
    $(document).ready(function () {
        $('#moreButton').click(function () {
            $('.rowCat').animate({height: "100%", overflow: "visible"}, 3200);
            $('#moreButton').toggleText('MEHR ANZEIGEN', 'WENIGER ANZEIGEN');
            $('#moreButton').toggleClass('fa-plus, fa-minus');
        });
    });
    $.fn.extend({
        toggleText: function (a, b) {
            return this.text(this.text() == b ? a : b);
        }
    });
</script>
<script>
    jQuery(document).ready(function () {
        $('.field').pwstrength();
    });
</script>
<!--END Show more Categorys Function -->
</body>
</html>
