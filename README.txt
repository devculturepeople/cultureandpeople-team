Strongly Typed by busy Guys ;)
chooo.media | Chris Matt
Free for personal and commercial use under the CCA 3.0 license


This is Strongly Typed, a new site template with a minimal, semi-retro
look (inspired by old instruction manuals) and, as you might guess from its
name, a strong emphasis on type. It's fully responsive, built on HTML5/CSS3,
and includes styling for all basic page elements.

Credits:
	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		jquery.dropotron (n33.co)
		skel (n33.co)