 <br><br>
 <div class="row highlightRow" style="margin: .5em 0px !important;">
            <div class="highlight">
                <section>
                    <header><a href="./?id=100"><div class="icon fa-calendar highlightIconBig"></div></a></header>
                    <footer><h3>Events verwalten</h3></footer>
                </section>
            </div>
            <div class="highlight">
                <section>
                    <header><a href="./?id=300"><div class="icon fa-id-card-o highlightIconBig"></div></a></header>
                    <footer><h3>Kategorien verwalten</h3></footer>
                </section>
            </div>
            <div class="highlight">
                <section>
                    <header><a href="./?id=200"><div class="icon fa-user highlightIconBig"></div></a></header>
                    <footer><h3>User verwalten</h3></footer>
                </section>
            </div>
            <div class="highlight">
                <section>
                    <header><a href="./?id=500"><div class="icon fa-gift highlightIconBig"></div></a></header>
                    <footer><h3>Gutscheine verwalten</h3></footer>
                </section>
            </div>
            <div class="highlight">
                 <section>
                     <header><a href="./?id=800"><div class="icon fa-gears highlightIconBig"></div></a></header>
                     <footer><h3>Account verwalten</h3></footer>
                 </section>
            </div>
            <div class="highlight">
                 <section>
                     <header><a href="./?logout"><div class="icon fa-sign-out highlightIconBig"></div></a></header>
                     <footer><h3>Logout</h3></footer>
                 </section>
            </div>
    </div>
 <br>
</div>
