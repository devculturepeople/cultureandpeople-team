-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 19. Dez 2016 um 22:51
-- Server Version: 5.6.21
-- PHP-Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `cap`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `firstpageslider`
--

CREATE TABLE IF NOT EXISTS `firstpageslider` (
`id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `event` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `firstpageslider`
--
ALTER TABLE `firstpageslider`
 ADD PRIMARY KEY (`id`);
